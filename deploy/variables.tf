variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "arizonatribe@gmail.com"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "ami_name" {
  default = "amzn2-ami-kernel-5.10-hvm-2.0.*.1-x86_64-gp2"
}

variable "db_username" {
  description = "Username for the RDS host instance"
}

variable "db_password" {
  description = "Password for the RDS host instance"
}
