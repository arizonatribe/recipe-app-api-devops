#!/bin/sh

set -e

# Working dir for this script will be the app/ directory, and manage.py is the Django entrypoint

# Collects all the static files in this project and co-locates them in a single directory
python manage.py collectstatic --noinput
# polls the db (every second) and once it is available, exits the command
python manage.py wait_for_db
# Execute database migrations
python manage.py migrate

# Use uwsgi to run the application, with multi-threading enabled
uwsgi --socket :9000 --workers 4 --master --enable-threads --module app.wsgi
